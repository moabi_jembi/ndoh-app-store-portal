import * as React from "react";
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
import LockOutline from "@material-ui/icons/LockOutlined"
import Dashboard from "@material-ui/icons/Dashboard";
import {IconVariants} from "../../enum";

import Erase from "@material-ui/icons/PhonelinkErase";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
import Apps from "@material-ui/icons/Apps";
import DataUsage from "@material-ui/icons/DataUsage";
import Profile from "@material-ui/icons/AccountBox";
import Add from "@material-ui/icons/Add";
import PlaylistAdd from "@material-ui/icons/PlaylistAdd";
import Application from "@material-ui/icons/Devices";
import Provider from "@material-ui/icons/Domain";
import Input from "@material-ui/icons/Input";

export const EraseIcon = Erase;
export const EditIcon = Edit;
export const CloseIcon = Close;
export const AppsIcon = Apps;
export const DataUsageIcon = DataUsage;
export const ProfileIcon= Profile;
export const ApplicationIcon= Application;
export const ProviderIcon = Provider;
export const ConnectIcon = Input;

const icons = {
  face: Face,
  email: Email,
  lock_outline: LockOutline,
  dashboard: Dashboard,
  add: Add,
  addMany: PlaylistAdd,
  connect: Input,
};

interface Props {
  variant: IconVariants;
  className?: string;
}

const Icon = (props:Props)=> {
  console.log(props);
  const {variant} = props;
  const TheIcon = icons[variant];
  return(
    <TheIcon />
  )
};

export default Icon;
