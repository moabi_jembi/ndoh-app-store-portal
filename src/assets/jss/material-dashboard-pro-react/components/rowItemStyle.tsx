import {
  defaultFont,
  primaryColor,
  dangerColor,
  grayColor,
  tooltip
} from "../../material-dashboard-pro-react";
import customCheckboxRadioSwitch from "../customCheckboxRadioSwitch";
import {Theme} from "@material-ui/core";
import createStyles from "@material-ui/core/styles/createStyles";

const rowItemStyle = (theme: Theme) => createStyles({
  ...customCheckboxRadioSwitch,
  table: {
    marginBottom: "0"
  },
  tableRow: {
    position: "relative",
    borderBottom: "1px solid " + grayColor[5]
  },
  tableActions: {
    border: "none",
    padding: "12px 8px !important",
    verticalAlign: "middle"
  },
  tableCell: {
    ...defaultFont,
    padding: "0",
    verticalAlign: "middle",
    border: "none",
    lineHeight: "1.42857143",
    fontSize: 14
  },
  tableActionButton: {
    width: "27px",
    height: "27px",
    padding: "0"
  },
  tableActionButtonIcon: {
    width: "17px",
    height: "17px"
  },
  edit: {
    backgroundColor: "transparent",
    color: primaryColor[0],
    boxShadow: "none"
  },
  close: {
    backgroundColor: "transparent",
    color: dangerColor[0],
    boxShadow: "none"
  },
  tooltip
});
export default rowItemStyle;
