import * as React from "react";
import {Route, Switch, Redirect, withRouter, RouteComponentProps} from "react-router-dom";

import AuthLayout from "./layouts/Auth.jsx";
import RtlLayout from "./layouts/RTL.jsx";
import AdminLayout from "./layouts/Admin.jsx";
import {connect} from "react-redux";
import {ApplicationState, User} from "./store/types";


interface WithStateToProps {
  user: User;
}

const mapStateToProps = (state: ApplicationState): WithStateToProps => {
  console.log(state);
  return {
    user: state.user,
  };
};

const mapDispatchToProps = {};

interface Props extends WithStateToProps, RouteComponentProps {
}

const App = (props: Props) => {
  const {user,location} = props;
  console.log(props);
  return user === null && !location.pathname.includes("/auth") ?
    <Redirect to="/auth/login-page" />:
    <Switch>
      <Route path="/auth" component={AuthLayout} />
      <Route path="/rtl" component={RtlLayout} />
      <Route path="/admin" component={AdminLayout} />
      <Redirect from="/" to="/admin/dashboard" />
    </Switch>
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
