

export type IconVariants =
  "connect" |
  "add" |
  "addMany" |
  "face" |
  "email" |
  "lock_outline" |
  "dashboard";
