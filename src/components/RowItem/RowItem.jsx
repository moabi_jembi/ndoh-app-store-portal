import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

import tasksStyle from "../../assets/jss/material-dashboard-pro-react/components/rowItemStyle";

class RowItem extends React.Component {

  render() {
    const { classes, items, actions, customCell } = this.props;
    console.log(customCell);
    const custom = customCell && customCell.length ? customCell : [];
    const actionItems = actions && actions.length ? actions : [];
    return (
      <Table className={ classes.table }>
        <TableBody>
          { items.map(value => (
            <TableRow
              key={ value }
              className={ classes.tableRow }
              style={
                {
                  display: "flex",
                  minHeight: 40,
                  alignItems: "center",
                } }>
              <TableCell className={ classes.tableCell } style={ {
                flex: 1,
              } }>
                { value }
              </TableCell>
              { custom.map((item, i) =>
                <TableCell
                  className={ classes.tableCell }
                  key={ i }
                >
                  { item }
                </TableCell>
              ) }
              <TableCell className={ classes.tableActions } style={ {
                display: "flex",
                justifyContent: "flex-end",
              } }>
                { actionItems.map((action, i) => {
                    const IconComponent = action.icon;
                    return <Tooltip
                      key={ i }
                      id="tooltip-top"
                      title={ action.tooltip }
                      placement="top"
                      classes={ { tooltip: classes.tooltip } }
                    >
                      <IconButton
                        aria-label="Edit"
                        className={ classes.tableActionButton }
                      >
                        <IconComponent
                          style={ {
                            color: action.color,
                          } }
                          className={
                            classes.tableActionButtonIcon
                          }
                        />
                      </IconButton>
                    </Tooltip>;
                  }
                ) }
              </TableCell>
            </TableRow>
          )) }
        </TableBody>
      </Table>
    );
  }
}

RowItem.propTypes = {
  classes: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.node),
  actions: PropTypes.arrayOf(PropTypes.node)
};

export default withStyles(tasksStyle)(RowItem);
