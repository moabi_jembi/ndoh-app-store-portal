import * as React from "react";
import * as ReactDOM from "react-dom";

import "assets/scss/material-dashboard-pro-react.scss?v=1.5.0";
import {Provider} from "react-redux";
import {store} from "./store";
import App from "./App";
import {Router} from "react-router";
import {createBrowserHistory} from "history";

const hist = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <App />
    </Router>
  </Provider>
  ,
  document.getElementById("root")
);
