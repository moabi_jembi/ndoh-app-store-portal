import axios from "axios";

export const callApi = (url: string, method: "GET" | "POST", postData: {} | null) => {
  return axios({
    method,
    url,
    data: postData
  })
  .then(res => {
    console.log(res);
    if (res.status === 401) {
      console.log("401");
    }
    if (res.status === 403) {
      console.log("403");
    }
    return res;
  })
  .catch((error) => {
    console.log(error.response);
    // if (error.response.status === 400 && error.response.data === "invalid_grant") {
    //   sessionStorage.removeItem(SessionStorageKeys.USER_CODE);
    //   window.location.replace(config.loginUrl)
    // } else if (error.response.status === 401) {
    //   accountLoginAction();
    //   console.log(error.response.data);
    // }
    return error.response;
  });
};
