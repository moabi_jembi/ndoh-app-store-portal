import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles, {WithStyles} from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";

// @material-ui/icons
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import CustomInput from "./../../../components/CustomInput/CustomInput.jsx";
import Button from "./../../../components/CustomButtons/Button.jsx";
import Card from "./../../../components/Card/Card.jsx";
import CardBody from "./../../../components/Card/CardBody.jsx";
import CardHeader from "./../../../components/Card/CardHeader.jsx";
import CardFooter from "./../../../components/Card/CardFooter.jsx";

import loginPageStyle from "./../../../assets/jss/material-dashboard-pro-react/views/loginPageStyle";
import Icon from "../../../assets/Icon";
import {WithLoginPageState} from "./LoginPage.container";
import {RouteComponentProps} from "react-router";

type Props = {}
  & WithLoginPageState
  & RouteComponentProps
  & WithStyles<typeof loginPageStyle>;

type State = {
  cardAnimation: "cardHidden",
  timeOutFunction: any
}

class LoginPageLayout extends React.Component<Props> {
  state: State = {
    cardAnimation: "cardHidden",
    timeOutFunction: setTimeout(
      () => {
        this.setState({cardAnimation: ""});
      }, 700)
  };

  componentWillUnmount() {
    const {
      timeOutFunction,
    } = this.state;
    clearTimeout(timeOutFunction);
    this.setState({timeOutFunction: null})
  }

  componentWillMount(): void {
    // callApi("http://magi.duckeggdigital.com/api/login", "POST", {}).then(res => {
    //   console.log(res);
    // })
  }

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.container}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={6} md={4}>
            <form>
              <Card login className={classes[this.state.cardAnimation]}>
                <CardHeader
                  className={`${classes.cardHeader} ${classes.textCenter}`}
                  color="primary"
                >
                  {/*<h5>Generated numbers collection:</h5>*/}
                  {/*<ul>*/}
                  {/*  {this.props.myNumberCollection.map((currentNumber) => <li key={currentNumber}>{currentNumber}</li>)}*/}
                  {/*</ul>*/}
                  {/*<Button onClick={this.props.onRequestNewNumber}>Request new number</Button>*/}
                  <h4 className={classes.cardTitle}>Log in</h4>
                </CardHeader>
                <CardBody>
                  <CustomInput
                    labelText="Email..."
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon variant="email" className={classes.inputAdornmentIcon} />
                        </InputAdornment>
                      )
                    }}
                  />
                  <CustomInput
                    labelText="Password"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon variant="lock_outline" className={classes.inputAdornmentIcon} />
                        </InputAdornment>
                      )
                    }}
                  />
                </CardBody>
                <CardFooter className={classes.justifyContentCenter}>
                  <Button color="rose" simple size="lg" block onClick={this.handleLoginButtonClick}>
                    Let's Go
                  </Button>
                </CardFooter>
              </Card>
            </form>
          </GridItem>
        </GridContainer>
      </div>
    );
  }

  handleLoginButtonClick = () => {
    console.log(this.props);
    const {history} = this.props;
    console.log("here");
    history.push('/admin/dashboard');
    // loadUsersRequest();
  }
}

export default withStyles(loginPageStyle)(LoginPageLayout);
