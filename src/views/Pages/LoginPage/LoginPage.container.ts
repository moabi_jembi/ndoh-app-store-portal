import {connect} from "react-redux";
import LoginPageLayout from "./LoginPage.layout";
import {Dispatch} from "redux";
import {ApplicationState, LoadingState, LoadUsersRequest, User} from "../../../store/types";
import {loadUsersRequest} from "../../../store/actions";

interface WithStateToProps {
  loading: LoadingState,
  user: User,
}

interface WithDispatchToProps {
  loadUsersRequest: ()=> LoadUsersRequest
}
export interface WithLoginPageState extends WithStateToProps, WithDispatchToProps {}

const mapStateToProps = (state: ApplicationState): WithStateToProps => ({
  loading: state.loading,
  user: state.user
});

const mapDispatchToProps = (dispatch: Dispatch): WithDispatchToProps => ({
  loadUsersRequest: () => dispatch(loadUsersRequest())
});

const LoginPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPageLayout);

export default LoginPage;
