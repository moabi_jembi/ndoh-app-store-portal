import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import CustomTabs from "../../components/CustomTabs/CustomTabs.jsx";


import { cardTitle, roseColor} from "../../assets/jss/material-dashboard-pro-react";
import { providers, traffic} from "../../variables/general";
import RowItem from "../../components/RowItem/RowItem";
import Icon, { ProviderIcon } from "../../assets/Icon";
import Button from "../../components/CustomButtons/Button";


const styles = {
  cardTitle,
  cardTitleWhite: {
    ...cardTitle,
    color: "#FFFFFF",
    marginTop: "0"
  },
  cardCategoryWhite: {
    margin: "0",
    color: "rgba(255, 255, 255, 0.8)",
    fontSize: ".875rem"
  },
  cardCategory: {
    color: "#999999",
    marginTop: "10px"
  },
  icon: {
    color: "#333333",
    margin: "10px auto 0",
    width: "130px",
    height: "130px",
    border: "1px solid #E5E5E5",
    borderRadius: "50%",
    lineHeight: "174px",
    "& svg": {
      width: "55px",
      height: "55px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      width: "55px",
      fontSize: "55px"
    }
  },
  iconRose: {
    color: roseColor
  },
  marginTop30: {
    marginTop: "30px"
  },
  testimonialIcon: {
    marginTop: "30px",
    "& svg": {
      width: "40px",
      height: "40px"
    }
  },
  cardTestimonialDescription: {
    fontStyle: "italic",
    color: "#999999"
  }
};

class Providers extends React.Component {
  render() {
    const {classes} = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12}>
            <Button color="info" className={classes.marginRight}>
              <Icon
                variant="add"
                className={classes.icons} /> Add Provider
            </Button>
          </GridItem>
          <GridItem xs={12}>
            <CustomTabs
              title=""
              headerColor="rose"
              tabs={[
                {
                  tabName: "Providers",
                  tabIcon: ProviderIcon,
                  tabContent: (
                    <RowItem
                      items={providers}
                      customCellData={traffic}
                    />
                  )
                },
              ]}
            />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(styles)(Providers);
