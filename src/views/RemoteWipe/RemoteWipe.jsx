import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// import Weekend from "@material-ui/icons/Weekend";
import PeopleIcon from "@material-ui/icons/People";
// core components
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import CustomTabs from "../../components/CustomTabs/CustomTabs.jsx";


import { cardTitle, dangerColor, roseColor } from "../../assets/jss/material-dashboard-pro-react";
import { users } from "../../variables/general";
import RowItem from "../../components/RowItem/RowItem";
import { EraseIcon } from "../../assets/Icon";


const styles = {
  cardTitle,
  cardTitleWhite: {
    ...cardTitle,
    color: "#FFFFFF",
    marginTop: "0"
  },
  cardCategoryWhite: {
    margin: "0",
    color: "rgba(255, 255, 255, 0.8)",
    fontSize: ".875rem"
  },
  cardCategory: {
    color: "#999999",
    marginTop: "10px"
  },
  icon: {
    color: "#333333",
    margin: "10px auto 0",
    width: "130px",
    height: "130px",
    border: "1px solid #E5E5E5",
    borderRadius: "50%",
    lineHeight: "174px",
    "& svg": {
      width: "55px",
      height: "55px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      width: "55px",
      fontSize: "55px"
    }
  },
  iconRose: {
    color: roseColor
  },
  marginTop30: {
    marginTop: "30px"
  },
  testimonialIcon: {
    marginTop: "30px",
    "& svg": {
      width: "40px",
      height: "40px"
    }
  },
  cardTestimonialDescription: {
    fontStyle: "italic",
    color: "#999999"
  }
};

class RemoteWipe extends React.Component {
  render() {
    return (
      <div>
        <GridContainer>
          <GridItem xs={12}>
            <CustomTabs
              title="Remote Wipe By:"
              headerColor="rose"
              tabs={[
                {
                  tabName: "User",
                  tabIcon: PeopleIcon,
                  tabContent: (
                    <RowItem
                      items={users}
                      actions={[
                        {
                          name: 'wipe',
                          icon: EraseIcon,
                          tooltip: 'Wipe user content',
                          color: dangerColor[0],
                          action: ()=>console.log('wipe me'),
                        },
                      ]}
                    />
                  )
                },
              ]}
            />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(styles)(RemoteWipe);
