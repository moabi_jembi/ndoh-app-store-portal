import {actionIds, LoadUsersError, LoadUsersRequest, LoadUsersSuccess, User} from "./types";

export const loadUsersRequest = (): LoadUsersRequest => ({
  type: actionIds.loadUsersRequest,
});

export const loadUsersSuccess = (users: User[]): LoadUsersSuccess => ({
  type: actionIds.loadUsersSuccess,
  users,
});

export const loadUsersError = (): LoadUsersError => ({
  type: actionIds.loadUsersError,
});
