import {call, put, takeEvery, all, fork} from "redux-saga/effects";
import {actionIds} from "../types";
import {callApi} from "../../services";

function* watchLoadUsersRequest() {
  yield takeEvery(actionIds.loadUsersRequest, requestLoadUsers);
}

function* requestLoadUsers() {
  // const generatedNumber = yield call(generateNewNumber);
  yield callApi("", "POST", null)
}


export const rootSaga = function* root() {
  yield all([
    fork(watchLoadUsersRequest),
  ])
};
