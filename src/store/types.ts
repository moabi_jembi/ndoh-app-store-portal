import {Action} from 'redux';

export enum actionIds {
  loadUsersRequest = '[0] Request a list of Users.',
  loadUsersSuccess = '[1] User list request success.',
  loadUsersError = '[2] User list request failed.',
}

interface UserInterface {
  name: string;
  age: number;
}

export type User = UserInterface | null;

export interface LoadingState {
  user: boolean;
}


export interface ApplicationState {
  loading: LoadingState;
  user: User;
}

export interface LoadUsersRequest extends Action {
  type: actionIds.loadUsersRequest;
}

export interface LoadUsersSuccess extends Action {
  type: actionIds.loadUsersSuccess;
  users: User[];
}

export interface LoadUsersError extends Action {
  type: actionIds.loadUsersError;
}

export type ApplicationAction =
  | LoadUsersRequest
  | LoadUsersSuccess
  | LoadUsersError;
