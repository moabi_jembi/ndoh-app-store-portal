import produce from "immer";
import {actionIds, ApplicationAction, ApplicationState} from "./types";

export const initialState: ApplicationState = {
  loading: {
    user: false,
  },
  user: {
    age: 38,
    name: "Claude",
  },
  // user: null
};

const reducer = (state = initialState, action: ApplicationAction) => {
  switch (action.type) {
    case actionIds.loadUsersRequest:
      return produce(state, draft => {
        draft.loading.user = true;
      });
    case actionIds.loadUsersSuccess:
      return produce(state, draft => {
        draft.loading.user = false;
        // draft.users = action.users;
      });
    case actionIds.loadUsersError:
      return produce(state, draft => {
        draft.loading.user = false;
      });
    default:
      return state;
  }
};

export default reducer;
