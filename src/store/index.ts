import {applyMiddleware, compose, createStore} from "redux";
import reducer, {initialState} from "./reducer";
import createSagaMiddleware from "redux-saga"
import {rootSaga} from "./sagas";

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(reducer, initialState,
  compose(
    applyMiddleware(sagaMiddleware),
    (<any>window)["__REDUX_DEVTOOLS_EXTENSION__"] && (<any>window)["__REDUX_DEVTOOLS_EXTENSION__"]()
  ));

sagaMiddleware.run(rootSaga);
